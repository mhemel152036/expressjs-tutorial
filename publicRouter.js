const express = require('express');

const publicRouter = express.Router();

const logger = (req, res, next) => {
    console.log("I'm logging somethings");
    next()
}

// publicRouter.param('uid', (req, res, next, uid) => {
//     const user = {
//         name: "Baten",
//         uid: uid
//     }
//     console.log("User :", user);
//     req.user = user;
//     next();
// });

publicRouter.param((param, option) => (req, res, next, val) => {
    if(option == val){
        next();
    }else{
        res.sendStatus(403);
    }
});

/* 
by tempering calling of param(),
we can pass value to the router.param((param, option) =>  )
*/
publicRouter.param('uid', 4); 

// publicRouter.use(logger);

publicRouter.all('*', logger);

publicRouter.get('/', (req, res) => {
    console.log('somethings');
    res.send('welcome to the public Homepage');
});

publicRouter.get('/post', (req, res) => {
    res.send('welcome to the public about page');
});

publicRouter.get('/user/:uid', (req, res) => {
    res.send(`Hello ${req.user.name} with Id. ${req.user.uid}`);
});



module.exports = publicRouter;
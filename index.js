const express = require('express');
const fs = require('fs');

const app = express();

/* Error handeling Types:
1. Synchronous - build in error handel mechanisum provides full error stack trace
2. Asynchronous
*/

// asynchronous error catch and hangle by custom erro handeler
app.get('/', (req, res, next) => {
    fs.readFile('/home1/desk/1.txt', (err, data) => {
        if(err){
            next(err);
        }else{
            res.send(data.toString());
        }
    });
});

app.get('/about', (rep, res) => {
    res.send("About page");
});

app.use((req, res, next) => {
/* 
This middle ware handeles 404 not found error,
beacuse 404- not found is not catch by any error handeler,
express app handels it and sends 404- not found status.

Tricks: if we want to catch such issues. We just needs to
write a middleware right after the route so that when any users
request doesn't match any route then The provided middleware will
catch it and will pass the error by providing error msg inside the
next()
*/
next("Requested url was nt found!");

});


app.use((err, req, res, next) => {
    if(err.message){
        res.status(500).send(err.message);
    }else{
        res.send("There was an error");
    }
});

app.listen(3000, () => {
    console.log("Listening on port 3000");
});